# 
# Stokes equations:
#    - laplace(u) + grad(p) = f
#    grad dot u = 0
#
#    u: unknown velocity
#    p: unknown pressure
#
# This program implements the unified Stokes algorithm using FEniCS
# 
# Method: 
# 	Loop:
# 		solve for u: a(u, v) - b(v, div(w)) = F(v) for u in V
# 		update w: w = rho*u + w for w^0 = 0 in V
#	solve for p: (p, q) = (div(w), q) for q in Pi^continuous
# 

from dolfin import *
import numpy

# create mesh										
mesh = UnitSquareMesh(8, 8, "crossed")	

# define velocity and pressure function space
u_element = "Lagrange"								
u_order = 4								
V = VectorFunctionSpace(mesh, u_element, u_order)			

# set boundary conditions
def boundary(x, on_boundary):
    return on_boundary
boundary_exp = Expression(("sin(4*pi*x[0])*cos(4*pi*x[1])", 
					 "-cos(4*pi*x[0])*sin(4*pi*x[1])")) # solution for u
bc = DirichletBC(V, boundary_exp, boundary)

# parameters for the Scott-Vogelius method
f = Expression(("28*pow(pi, 2)*sin(4*pi*x[0])*cos(4*pi*x[1])", 
				"-36*pow(pi, 2)*cos(4*pi*x[0])*sin(4*pi*x[1])"))

# rate of convergence
r = 1.0e3

# define test and trial functions
u = TrialFunction(V)
v = TestFunction(V)

# define pressure stand-in 
w = Function(V)

# set the variational problem
a = inner(grad(u), grad(v))*dx + r*div(u)*div(v)*dx
L = inner(f, v)*dx + div(w)*div(v)*dx

u = Function(V)
pde = LinearVariationalProblem(a, L, u, bc)
solver = LinearVariationalSolver(pde)

# Scott-Vogelius iterated penalty method
iters = 0; max_iters = 10; div_u_norm = 1 
while iters < max_iters and div_u_norm > 1e-10:

	# solve and update w
	solver.solve()
	w.vector().axpy(-r, u.vector()) 

	# find the L^2 norm of div(u) to check stopping condition
	div_u_norm = sqrt(assemble(div(u)*div(u)*dx(mesh)))
	iters += 1

p_US = project(div(w), FunctionSpace(mesh, "Lagrange", u_order-1))

# plot the solution
plot(u)
















